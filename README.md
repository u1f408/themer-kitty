# themer-kitty

[![Build Status](https://travis-ci.org/0x52a1/themer-kitty.svg?branch=master)](https://travis-ci.org/0x52a1/themer-kitty)

A [kitty][] theme template for [themer][].

## Installation and usage

```
npm install themer-kitty
```

Then pass `themer-kitty` as a template to themer when generating:

```
themer -c my-colors.js -t themer-kitty -o gen
```

## Output

`themer-kitty` will generate a file for each enabled colorscheme (light or dark), which will be in the output directory as `light.config` or `dark.config`.

Pick the one you want to use and copy it's contents into your `$HOME/.config/kitty/kitty.conf` file (or `~/Library/Preferences/kitty/kitty.conf` on macOS).

## License

MIT, see [LICENSE](./LICENSE).

[kitty]: https://github.com/kovidgoyal/kitty
[themer]: https://github.com/mjswensen/themer
