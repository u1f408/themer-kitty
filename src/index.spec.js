// vim: set ts=2 sw=2 expandtab :
import { render } from './index'
import { colors } from 'themer-colors-default'

describe('themer-kitty', () => {

  it('should produce multiple files, one for each scheme', async () => {
    const files = await Promise.all(render(colors, {colors: 'themer-colors-default'}))
    expect(files.length).toBe(Object.keys(colors).length)

    for (var i in files) {
      const scheme = files[i].name.split('.')[0]

      const fileContents = files[i].contents.toString('utf8')
      expect(/undefined/.test(fileContents)).toBe(false)
      expect((new RegExp(scheme)).test(fileContents)).toBe(true)
    }
  });


  const testSingleScheme = (message, colors) => {
    it(message, async () => {
      const files = await Promise.all(render(colors, {colors: 'themer-colors-default'}))
      expect(files.length).toBe(Object.keys(colors).length)

      const scheme = files[0].name.split('.')[0]

      const fileContents = files[0].contents.toString('utf8')
      expect(/undefined/.test(fileContents)).toBe(false)
      expect((new RegExp(scheme)).test(fileContents)).toBe(true)
    });
  };


  testSingleScheme('should produce only one file, for the dark scheme', { dark: colors.dark });
  testSingleScheme('should produce only one file, for the light scheme', { light: colors.light });

});
